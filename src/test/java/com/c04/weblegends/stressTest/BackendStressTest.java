package com.c04.weblegends.stressTest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class BackendStressTest {
    int iteration = 1000;
    HttpURLConnection http;
    Map<String, String> player;

    public HttpURLConnection getUrl(String target, String method) throws Exception {
        URL url = new URL(target);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection)con;
        http.setRequestMethod(method);
        http.setDoOutput(true);
        return http;
    }

    public Map<String, String> addPlayer(HttpURLConnection http, String name, String heroClass) throws Exception {

        byte[] out = ("{\"name\":\"" + name + "\",\"heroClass\":\"" + heroClass + "\"}").getBytes(StandardCharsets.UTF_8);
        int length = out.length;

        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        try(OutputStream os = http.getOutputStream()) {
            os.write(out);
        }

        InputStreamReader in = new InputStreamReader((InputStream) http.getContent());
        BufferedReader buff = new BufferedReader(in);
        Map<String, String> retVal = this.stringToHashMap(buff.readLine());

        return retVal;
    }

    public Map<String, String> stringToHashMap(String in) {
        Map<String, String> res = new HashMap<String, String>();
        in = in.substring(1, in.length()-1); // remove { ... }
        String[] pairs = in.split(",");
        for (int i=0;i<pairs.length;i++) {
            String pair = pairs[i];
            String[] keyValue = pair.split(":");
            keyValue[0] = keyValue[0].substring(1, keyValue[0].length()-1);
            if(keyValue[0].equalsIgnoreCase("score")) {
                keyValue[1] = keyValue[1].toString(); 
            }
            else {
                keyValue[1] = keyValue[1].substring(1, keyValue[1].length()-1);
            }
            res.put(keyValue[0], keyValue[1]);
        }

        return res;
    }
    
    @Test
    public void stressTest() throws Exception {

        System.out.println("Perform stress test on get all player");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/player/", "GET");
            this.http.getResponseCode();
        }

        System.out.println("Perform stress test on create new player");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/player/", "POST");
            this.player = this.addPlayer(this.http, "qwerty", "Test");
            
            this.http = this.getUrl("http://localhost:8080/player/" + this.player.get("id"), "DELETE");
            this.http.getResponseCode(); // ga tau kenapa ini mesti ada biar bisa ke send

        }

        this.http = this.getUrl("http://localhost:8080/player/", "POST");
        this.player = this.addPlayer(this.http, "qwerty", "Test");

        System.out.println("Perform stress test on get player by id");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/player/" + this.player.get("id"), "GET");
            this.http.getResponseCode();
        }

        System.out.println("Perform stress test on get player by name");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/player/name/" + this.player.get("name"), "GET");
            this.http.getResponseCode();
        }

        this.http = this.getUrl("http://localhost:8080/player/" + this.player.get("id"), "DELETE");
        this.http.getResponseCode();


        this.http = this.getUrl("http://localhost:8080/player/", "POST");
        this.player = this.addPlayer(this.http, "qwerty", "Test");
        this.http = this.getUrl("http://localhost:8080/player/", "POST");
        Map<String, String> player2 = this.addPlayer(this.http, "qwerty1", "Test");

        System.out.println("Perform stress test on get all player by hero class");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/hero-class/" + this.player.get("heroClass"), "GET");
            this.http.getResponseCode();
        }

        System.out.println("Perform stress test on get all player by 2 top score");
        for(int i = 0; i < this.iteration; i++) {
            this.http = this.getUrl("http://localhost:8080/player/top-score/" + 2, "GET");
            this.http.getResponseCode();
        }

        this.http = this.getUrl("http://localhost:8080/player/" + this.player.get("id"), "DELETE");
        this.http.getResponseCode();
        this.http = this.getUrl("http://localhost:8080/player/" + player2.get("id"), "DELETE");
        this.http.getResponseCode();


        this.http = this.getUrl("http://localhost:8080/player/", "POST");
        this.player = this.addPlayer(this.http, "qwerty", "Test");

        System.out.println("Perform stress test on update player name");
        for(int i = 0; i < this.iteration/2; i++) {
            this.http = this.getUrl("http://localhost:8080/player/update/" + this.player.get("id") + "/name/" + this.player.get("name"), "GET");
            this.http.getResponseCode();

            this.http = this.getUrl("http://localhost:8080/player/update/" + this.player.get("id") + "/name/" + "SesuatuYgLain", "GET");
            this.http.getResponseCode();
        }

        System.out.println("Perform stress test on update player score");
        for(int i = 0; i < this.iteration/2; i++) {
            this.http = this.getUrl("http://localhost:8080/player/update/" + this.player.get("id") + "/score/" + i, "GET");
            this.http.getResponseCode();
        }

        this.http = this.getUrl("http://localhost:8080/player/" + this.player.get("id"), "DELETE");
        this.http.getResponseCode();

    }
}

