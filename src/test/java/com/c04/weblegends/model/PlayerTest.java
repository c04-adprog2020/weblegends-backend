package com.c04.weblegends.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {
    Player player;
    Player player2;

    /**
     * Set UP for every test.
     */
    @BeforeEach
    public void setUp() {
        this.player = new Player(UUID.fromString("92984265-b834-45d9-8fd8-9954654cdbfb"),
                                "Testing",
                                1000,
                                "Beta");

        this.player2 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                "Testing2",
                                "Beta");
    }

    @Test
    public void testGetter() {
        assertEquals(UUID.fromString("92984265-b834-45d9-8fd8-9954654cdbfb"), this.player.getId());
        assertEquals("Testing", this.player.getName());
        assertEquals(1000, this.player.getScore());
        assertEquals("Beta", this.player.getHeroClass());
        
        assertEquals(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"), this.player2.getId());
        assertEquals("Testing2", this.player2.getName());
        assertEquals(0, this.player2.getScore());
        assertEquals("Beta", this.player2.getHeroClass());
    }

    @Test
    public void testSetter() {
        this.player.setName("TestingNew");
        this.player.setScore(1001);
        this.player.setHeroClass("Beta2");
        assertEquals("TestingNew", this.player.getName());
        assertEquals(1001, this.player.getScore());
        assertEquals("Beta2", this.player.getHeroClass());
        
        this.player2.setName("TestingNew2");
        this.player2.setScore(1001);
        this.player2.setHeroClass("Beta2");
        assertEquals("TestingNew2", this.player2.getName());
        assertEquals(1001, this.player2.getScore());
        assertEquals("Beta2", this.player2.getHeroClass());
    }
}