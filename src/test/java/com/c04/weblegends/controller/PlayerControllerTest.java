package com.c04.weblegends.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.c04.weblegends.model.Player;
import com.c04.weblegends.service.PlayerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(controllers = PlayerController.class)
public class PlayerControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;

    Player mockPlayer = new Player(UUID.fromString("92984265-b834-45d9-8fd8-9954654cdbfb"),
                                    "Testing",
                                    1000,
                                    "Beta");
    
    @Test
    public void testFindAll() throws Exception {

        Player mockPlayer2 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                        "Testing2",
                                        900,
                                        "Beta");
        
        List<Player> playerList = new ArrayList<Player>();
        playerList.add(mockPlayer);
        playerList.add(mockPlayer2);
        
        Mockito.when(playerService.findAll()).thenReturn(playerList);

        String uri = "/player";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(playerList);

        String outputInJson = result.getResponse().getContentAsString();

        assertThat(outputInJson).isEqualTo(expectedJson);
    }
    
    @Test
    public void testFindPlayer() throws Exception {

        Optional<Player> player = Optional.of(mockPlayer);

        Mockito.when(
                    playerService.findPlayerById(
                        UUID.fromString("92984265-b834-45d9-8fd8-9954654cdbfb")
                    )
                )
               .thenReturn(player);

        String uri = "/player/92984265-b834-45d9-8fd8-9954654cdbfb";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(player.get());
        String outputInJson = result.getResponse().getContentAsString();

        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void testFindAllByHeroClass() throws Exception {

        Player mockPlayer2 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                        "Testing2",
                                        900,
                                        "Beta");

        List<Player> playerList = new ArrayList<Player>();
        playerList.add(mockPlayer);
        playerList.add(mockPlayer2);
        

        Mockito.when(playerService.findAllByHeroClass(Mockito.anyString()))
               .thenReturn(playerList);

        String uri = "/player/hero-class/Beta";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(playerList);
        String outputInJson = result.getResponse().getContentAsString();

        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void testFindTopScore() throws Exception {

        Player mockPlayer2 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                        "Testing2",
                                        900,
                                        "Beta");
        Player mockPlayer3 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                        "Testing3",
                                        800,
                                        "Beta");
        Player mockPlayer4 = new Player(UUID.fromString("3559aefb-9e31-401b-b598-e770ec2948a4"),
                                        "Testing4",
                                        700,
                                        "Beta");

        List<Player> playerList = new ArrayList<Player>();
        playerList.add(mockPlayer);
        playerList.add(mockPlayer2);
        playerList.add(mockPlayer3);
        playerList.add(mockPlayer4);

        Mockito.when(playerService.findTopScore(10)).thenReturn(playerList);

        String uri = "/player/top-score/10";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(playerList);
        String outputInJson = result.getResponse().getContentAsString();

        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    @Test
    public void testUpdatePlayerName() throws Exception {

        String uri = "/player/update/92984265-b834-45d9-8fd8-9954654cdbfb/name/TestingNew";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testUpdatePlayerScore() throws Exception {

        String uri = "/player/update/92984265-b834-45d9-8fd8-9954654cdbfb/score/1100";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
                
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testAddPlayer() throws Exception {

        String input = "{\"id\":\"92984265-b834-45d9-8fd8-9954654cdbfb\","
                     + "\"name\":\"Testing\","
                     + "\"score\":1000,"
                     + "\"heroClass\":\"Beta\"}";

        String uri = "/player";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(uri)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);
                
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testDeletePlayer() throws Exception {

        String input = "{\"id\":\"92984265-b834-45d9-8fd8-9954654cdbfb\","
                     + "\"name\":\"Testing\","
                     + "\"score\":1000,"
                     + "\"heroClass\":\"Beta\"}";

        String uri = "/player";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uri)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);
        
        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri + "/92984265-b834-45d9-8fd8-9954654cdbfb")
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mockMvc.perform(requestBuilderDelete).andExpect(status().isOk());

    }

    @Test
    public void testFindIdByName() throws Exception { 

        String input = "{\"id\":\"92984265-b834-45d9-8fd8-9954654cdbfb\","
                     + "\"name\":\"Testing\","
                     + "\"score\":1000,"
                     + "\"heroClass\":\"Beta\"}";
                     
        String uri = "/player";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uri)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri + "/name/Testing")
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }


    /**
    * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
    * Source: Internet.
    */
    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}