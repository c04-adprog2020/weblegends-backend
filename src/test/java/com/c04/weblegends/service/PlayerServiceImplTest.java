package com.c04.weblegends.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.c04.weblegends.model.Player;
import com.c04.weblegends.repository.PlayerRepository;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PlayerServiceImplTest {

    @Mock
    PlayerRepository  playerRepository;

    @Mock
    Player mockPlayer;

    @InjectMocks
    PlayerServiceImpl playerServiceImpl;
    
    UUID id = UUID.fromString("92984265-b834-45d9-8fd8-9954654cdbfb");
    
    @Test
    public void testAddPlayer() {
        Player player = new Player();
        playerServiceImpl.addPlayer(player);

        verify(playerRepository, times(1)).save(player);
    }

    @Test
    public void testFindAll() {
        playerServiceImpl.findAll();

        verify(playerRepository, times(1)).findAll();
    }

    @Test
    public void testFindPlayerById() {
        playerServiceImpl.findPlayerById(id);

        verify(playerRepository, times(1)).findById(id);
    }

    @Test
    public void testDeletePlayerById() {
        playerServiceImpl.deletePlayerById(id);

        verify(playerRepository, times(1)).deleteById(id);
    }

    @Test
    public void testUpdatePlayerNameById() {
        Player player = new Player(id, "test", "beta");

        Mockito.when(playerServiceImpl.findPlayerById(id)).thenReturn(Optional.of(player));
        
        playerServiceImpl.updatePlayerNameById(id, "test");

        verify(playerRepository, times(1)).save(player);
    }

    @Test
    public void testUpdatePlayerScoreById() {
        
        Player player = new Player(id, "test", "beta");

        Mockito.when(playerServiceImpl.findPlayerById(id)).thenReturn(Optional.of(player));

        playerServiceImpl.updatePlayerScoreById(id, (long)1);

        verify(playerRepository, times(1)).save(player);
    }

    @Test
    public void testFindAllByHeroClass() {
        playerServiceImpl.findAllByHeroClass("beta");

        verify(playerRepository, times(1)).findAllByHeroClass("beta");
    }

    @Test
    public void testFindTopTenScore() {
        playerServiceImpl.findTopScore(10);

        verify(playerRepository, times(1)).findTopScore(10);
    }

    @Test
    public void testFindPlayerByName() {
        Player player = new Player(id, "test", "beta");
        Mockito.when(playerRepository.findPlayerByName("test")).thenReturn(Optional.of(player));

        playerServiceImpl.findPlayerByName("test");

        verify(playerRepository, times(1)).findPlayerByName("test");
    }
    
}