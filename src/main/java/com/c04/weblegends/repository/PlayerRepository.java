package com.c04.weblegends.repository;

import com.c04.weblegends.model.Player;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

@Component("postgre")
public interface PlayerRepository extends JpaRepository<Player, UUID> {
    public List<Player> findAllByHeroClass(String heroClass);

    @Query(value = "SELECT * FROM player ORDER BY score DESC LIMIT (?1);", nativeQuery = true)
    public List<Player> findTopScore(int limit);
    
    @Query(value = "SELECT * FROM player WHERE name = (?1);", nativeQuery = true)
    public Optional<Player> findPlayerByName(String name);
}