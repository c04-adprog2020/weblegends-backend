package com.c04.weblegends.controller;

import com.c04.weblegends.model.Player;
import com.c04.weblegends.service.PlayerService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/player")
public class PlayerController {

    @Autowired
    private PlayerService playerServiceImpl;


    public PlayerController(PlayerService playerServiceImpl) {
        this.playerServiceImpl = playerServiceImpl;
    }

    @GetMapping
    public List<Player> findAll() {
        return playerServiceImpl.findAll();
    }
    
    @PostMapping
    public Optional<Player> addPlayer(@RequestBody Player player) {
        return playerServiceImpl.addPlayer(player);
    }
    
    @DeleteMapping("/{id}")
    public void deletePlayer(@PathVariable("id") UUID id) {
        playerServiceImpl.deletePlayerById(id);
    }
    
    @GetMapping("/{id}")
    public Optional<Player> findPlayer(@PathVariable("id") UUID id) {
        return playerServiceImpl.findPlayerById(id);
    }

    /**
     * Method for search the id of player with given name.
     * @param name A player name that want to search.
     * @return the id of player.
     */
    @GetMapping("/name/{playerName}")
    public Optional<Player> findPlayerByName(@PathVariable("playerName") String name) {
        return playerServiceImpl.findPlayerByName(name);
    }
    
    @GetMapping("/hero-class/{heroClass}")
    public List<Player> findAllByHeroClass(@PathVariable("heroClass") String heroClass) {
        return playerServiceImpl.findAllByHeroClass(heroClass);
    }
    
    @GetMapping("/top-score/{limit}")
    public List<Player> findTopScore(@PathVariable("limit") int limit) {
        return playerServiceImpl.findTopScore(limit);
    }

    @GetMapping("/update/{id}/name/{newName}")
    public void updatePlayerName(@PathVariable("id") UUID id,
                                 @PathVariable("newName") String newName) {
        playerServiceImpl.updatePlayerNameById(id, newName);
    }

    @GetMapping("/update/{id}/score/{newScore}")
    public void updatePlayerScore(@PathVariable("id") UUID id,
                                  @PathVariable("newScore") Long newScore) {
        playerServiceImpl.updatePlayerScoreById(id, newScore);
    }
}