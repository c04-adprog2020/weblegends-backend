package com.c04.weblegends.model;


import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "score")
    private long score = 0;

    @Column(name = "hero_class")
    private String heroClass;

    /**
    * This is a empty constructor for JPA to work.
    */
    public Player() {

    }
    
    /**
    * This is a constructor with default score is 0.
    */
    public Player(UUID id, String name, String heroClass) {
        this.id = id;
        this.name = name;
        this.heroClass = heroClass;
    }

    /**
    * This is a constructor when you need to set the default score.
    */
    public Player(UUID id, String name,long score, String heroClass) {
        this.id = id;
        this.name = name;
        this.score = score;
        this.heroClass = heroClass;
    }

    public UUID getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getScore() {
        return this.score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getHeroClass() {
        return this.heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

}