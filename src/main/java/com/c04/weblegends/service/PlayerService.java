package com.c04.weblegends.service;

import com.c04.weblegends.model.Player;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public interface PlayerService {

    public Optional<Player> addPlayer(Player player);
    
    public List<Player> findAll();

    public Optional<Player> findPlayerById(UUID id);
    
    public void deletePlayerById(UUID id);

    public void updatePlayerNameById(UUID id, String newName);

    public void updatePlayerScoreById(UUID id, Long newScore);

    public List<Player> findAllByHeroClass(String heroClass);

    public List<Player> findTopScore(int limit);

    public Optional<Player> findPlayerByName(String name);
}