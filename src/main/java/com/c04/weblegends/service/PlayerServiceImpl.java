package com.c04.weblegends.service;

import com.c04.weblegends.model.Player;
import com.c04.weblegends.repository.PlayerRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    @Qualifier("postgre")
    private final PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    /** 
     * A method to register a player to the database.
     * 
     * @param player accept a objet player after converted from json.
     * @return id of the player that generated automaticaly when player is saved to db. 
     */
    public Optional<Player> addPlayer(Player player) {
        playerRepository.save(player);
        Optional<Player> registeredPlayer = playerRepository.findById(player.getId());
        return registeredPlayer;
    }
    
    public List<Player> findAll() {
        return playerRepository.findAll();
    }

    public Optional<Player> findPlayerById(UUID id) {
        return playerRepository.findById(id);
    }
    
    public void deletePlayerById(UUID id) {
        playerRepository.deleteById(id);
    }

    /** 
     * A method to change the player name by given id.
     * 
     * @param id the id of player that wanted to change the name.
     * @param newName the new name.
     */
    public void updatePlayerNameById(UUID id, String newName) {
        Player player = this.findPlayerById(id).get();
        player.setName(newName);
        playerRepository.save(player);
    }

    /** 
     * A method to change or put new score of player by given id.
     * 
     * @param id the id of player that wanted to change the name.
     * @param newScore the new score.
     */
    public void updatePlayerScoreById(UUID id, Long newScore) {
        Player player = this.findPlayerById(id).get();
        player.setScore(newScore);
        playerRepository.save(player);
    }

    public List<Player> findAllByHeroClass(String heroClass) {
        return playerRepository.findAllByHeroClass(heroClass);
    }

    public List<Player> findTopScore(int limit) {
        return playerRepository.findTopScore(limit);
    }

    /** 
     * A method to get the id of player by given name.
     * 
     * @param name  the name that wanted to know the id.
     */
    public Optional<Player> findPlayerByName(String name) {
        return playerRepository.findPlayerByName(name);
    }
}