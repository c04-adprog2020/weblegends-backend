[![pipeline status](https://gitlab.com/c04-adprog2020/weblegends-backend/badges/master/pipeline.svg)](https://gitlab.com/c04-adprog2020/weblegends-backend/-/commits/master) [![coverage report](https://gitlab.com/c04-adprog2020/weblegends-backend/badges/master/coverage.svg)](https://gitlab.com/c04-adprog2020/weblegends-backend/-/commits/master)
# Database Manager Service
This is an API for store and retrieve a data with the intention to be a [Web Legend Database Manager Service](https://c04-database-manager-service.herokuapp.com/).

# Features
|#|Path|Method|Return Type|Desc|
|---|:---|---|---|---|
|1| `player`| **GET** |`List<Player>`| Get all the player that in the database.|
|2| `player`| **POST** |`Optional<Player>`| Register a player to the database. This require a json object of the `name` and `heroClass`, if needed `score` can be include or the default is **0**.|
|3| `player/{id}`| **DELETE** |`void`| Delete a player with given id from database.|
|4| `player/{id}`| **GET** |`Optional<Player>`| Get a player by an id.|
|5| `player/name/{playerName}`| **GET** |`Optional<Player>`| Get a player by a name.|
|6| `player/hero-class/{heroClass}`| **GET** |`List<Player>`| Get all player with the same hero class.|
|7| `player/top-score/{limit}`| **GET** |`List<Player>`| Get all player with highest score which the amount is limited with `limit`.|
|8| `player/update/{id}/name/{newName}`| **GET** |`void`| Update a player name with id is `id` to `newName`.|
|9| `player/update/{id}/score/{newScore}`| **GET** |`void`| Update a player score with id is `id` to `newScore`.|
